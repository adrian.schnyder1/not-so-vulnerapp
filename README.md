# Vulnerapp (not anymore)

### Warum meine CSRF implementation funktioniert:

Ich gebe mit JS im header eine X-XSRF-TOKEN mit, ausserdem habe ich im file `VulnerApplication` in der Methode FilterChain, die nötige Konfiguration geschrieben, so dass Spring die CSRF validierung selbst handlen kann.

### Reflektion:

Bei der Analyse der gegebenen Sicherheitsanforderungen wurde festgestellt, dass die Applikation verschiedene Sicherheitslücken aufweist, die dringend behoben werden müssen. Das grundlegende Ziel besteht darin, die Sicherheit der Applikation zu verbessern, ohne das grundlegende Verhalten zu beeinträchtigen. Im Folgenden werden die implementierten Sicherheitsmechanismen diskutiert, um die Sicherheit der Applikation zu erhöhen.

    Verwendung von korrekten REST-Verben:
    Die Applikation wurde so angepasst, dass korrekte REST-Verben verwendet werden. Dadurch wird sichergestellt, dass die Datenmanipulation über die API gemäß den RESTful-Prinzipien durchgeführt wird. Dies hilft, die Sicherheit zu verbessern, indem unnötige und unsichere Operationen vermieden werden. Zum Beispiel

    Implementierung einer Authentifizierungslösung:
    Eine Authentifizierungslösung über BasicAuth, wurde implementiert. Dies ermöglicht es, dass nur autorisierte Benutzer auf gewisse Funktionen der Applikationen zugreifen können. Durch die Überprüfung der Identität und Zugangsberechtigung wird die Sicherheit erhöht und unautorisierte Zugriffe verhindert.

    Enforce RBAC:
    Eine RBAC (Rollenbasierte Zugriffskontrolle) wurde implementiert, um zwischen Benutzer- und Administratordiensten zu unterscheiden. Dadurch werden unterschiedliche Zugriffsberechtigungen basierend auf den zugewiesenen Rollen gewährt. Dies hilft, sicherzustellen, dass nur autorisierte Benutzer bestimmte Aktionen ausführen können und schützt vor unbefugten Zugriffen.

    Aktivierung von CSRF-Protection:
    CSRF-Protection (Cross-Site Request Forgery) wurde aktiviert. Diese Implementierung stellt sicher, dass Anfragen nur von vertrauenswürdigen Quellen stammen und nicht von bösartigen Websites oder Angreifern generiert wurden. Durch die Verwendung von Sicherheitstokens und Überprüfung der Herkunft der Anfragen wird die Sicherheit gegen CSRF-Angriffe erhöht.

    Implementierung einer sicheren Passwort-Speicherung:
    Für die sichere Passwort-Speicherung wurde Hashing verwendet. Die Passwörter werden vor der Speicherung gehasht, um sicherzustellen, dass sie nicht im Klartext gespeichert werden. Zusätzlich wurden Passwortregeln implementiert, um die Komplexität der Passwörter zu erhöhen und vor schwachen Passwörtern zu schützen.

    Strikte Inputvalidierung:
    Um potenzielle Sicherheitslücken wie SQL-Injection (SQLi) und Cross-Site Scripting (XSS) zu verhindern, wurden strikte Inputvalidierungen implementiert. Dies stellt sicher, dass alle eingegebenen Daten ordnungsgemäß überprüft und bereinigt werden, um schädlichen Code oder unerwünschte Datenmanipulation zu verhindern.

    Behebung der initialen Sicherheitslücken:
    Die anfänglichen Sicherheitslücken, wie SQL-Injection (SQLi), Cross-Site Scripting (XSS) und CSRF, wurden behoben. Durch die Implementierung der entsprechenden Sicherheitsmechanismen(html-stripping) und Validierungen wurde sichergestellt, dass diese Schwachstellen nicht mehr ausgenutzt werden können.

#### Zusätzliche Sicherheitsmechanismen:

Es gibt noch weitere mögliche Sicherheitsmechanismen, die implementiert werden könnten, um die Sicherheit der Applikation weiter zu stärken. Einige Beispiele sind:

    Implementierung von Zwei-Faktor-Authentifizierung (2FA) für zusätzliche Sicherheitsschichten.
    Verwendung von HTTPS (SSL/TLS) für eine sichere Datenübertragung zwischen Client und Server.
    Implementierung von Sicherheitsüberwachung und Protokollierung, um verdächtige Aktivitäten zu erkennen und darauf zu reagieren.
    Durchführung von regelmäßigen Sicherheitsaudits und Penetrationstests, um potenzielle Schwachstellen zu identifizieren und zu beheben.

#### Potenzielle Schwierigkeiten und Verbesserungen:

Eine schwäche der Applikation ist ihre Anfälligkeit gegen Brute force attacken. Die hätte gefixxt werden können, indem Nutzer gesperrt werden sobald mehr als z.B. 20 fehlgeschlagene Login versüche auf dieses Konto eingehen oder indem IP-Adressen gesperrt werden welche zu viele gescheiterte Login versuche abgeschickt haben.

#### Schwierigkeiten bei der Entwicklung

Wie bei schon so vielen Projekten fand ich das Entwickeln mit Java nicht sehr angenehm, das Modul hingegen fand ich interessant und ich konnte mit den verschiedenen Konzepten gut mitkommen. Meine fehlende Erfahrung mit Java konnte ich einigermassen kompensieren indem eng mit Leo zusammenarbeitete und mir Tipps einholen konnte. Das Unterrichts Format indem wir jeden Montag ein neues Feature implementierten war auch äusserst hilfreich.
