package ch.bbw.m183.vulnerapp;

import ch.bbw.m183.vulnerapp.service.AdminService;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VulnerApplicationTests implements WithAssertions {

	@Autowired
	WebTestClient webTestClient;

	@MockBean
	private AdminService adminService;


	@Test
	void blogsArePublic() {
		webTestClient.get().uri("/api/blog")
				.exchange()
				.expectStatus().isOk();
	}

	@Test
	void usersArePrivateForUnauthorized() {
		webTestClient.get().uri("/api/admin123/users")
				.exchange()
				.expectStatus()
				.isUnauthorized();
	}

	@Test
	void usersAreAccesibleToAdmins() {
		webTestClient.get()
				.uri("/api/users")
				.header("ROLE", "ADMIN")
				.exchange()
				.expectStatus()
				.isOk();
	}

	@Test
	void contextLoads() {
	}
}
