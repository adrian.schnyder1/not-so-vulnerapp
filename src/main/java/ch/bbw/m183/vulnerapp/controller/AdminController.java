package ch.bbw.m183.vulnerapp.controller;

import ch.bbw.m183.vulnerapp.XSS;
import ch.bbw.m183.vulnerapp.datamodel.UserEntity;
import ch.bbw.m183.vulnerapp.service.AdminService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin123") // noone will ever guess!
@RequiredArgsConstructor
@Validated
public class AdminController {

	private final AdminService adminService;

	@PostMapping("/create")
	@PreAuthorize("hasRole('ADMIN')")
	public UserEntity createUser(@RequestBody @Valid UserEntity newUser) {
		newUser.setUsername(XSS.StripHTML(newUser.getUsername())).setFullname(XSS.StripHTML(newUser.getFullname())).setPassword(newUser.getPassword());
		return adminService.createUser(newUser);
	}

	@GetMapping("/users")
	@PreAuthorize("hasRole('ADMIN')")
	public Page<UserEntity> getUsers(Pageable pageable) {
		return adminService.getUsers(pageable);
	}

	@DeleteMapping("/delete/{username}")
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteUser(@PathVariable String username) {
		adminService.deleteUser(username);
	}
}
