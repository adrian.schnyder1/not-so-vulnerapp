package ch.bbw.m183.vulnerapp;

public class Const {
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
}
