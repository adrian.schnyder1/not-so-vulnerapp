package ch.bbw.m183.vulnerapp;


import org.springframework.web.util.HtmlUtils;

public class XSS {

    private XSS() {}


    public static String StripHTML(String input) {
        return HtmlUtils.htmlEscape(input);
    }
}
