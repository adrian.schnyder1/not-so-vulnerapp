package ch.bbw.m183.vulnerapp.service;

import ch.bbw.m183.vulnerapp.datamodel.UserEntity;
import ch.bbw.m183.vulnerapp.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class LoginService {

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	public ResponseEntity<UserEntity> whoami(String username, String password) {
		var user = userRepository.findById(username).orElse(new UserEntity().setUsername("idk"));
		if (passwordEncoder.matches(password, user.getPassword())) {
			return ResponseEntity.ok(user);
		}
		return ResponseEntity.status(403).body(user);
	}
}
