package ch.bbw.m183.vulnerapp;

import ch.bbw.m183.vulnerapp.datamodel.UserEntity;
import ch.bbw.m183.vulnerapp.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestHandler;
import org.springframework.security.web.csrf.XorCsrfTokenRequestAttributeHandler;

import java.util.*;

import static ch.bbw.m183.vulnerapp.Const.ROLE_ADMIN;
import static ch.bbw.m183.vulnerapp.Const.ROLE_USER;
import static org.springframework.security.config.Customizer.withDefaults;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@Configuration
@EnableWebSecurity
public class VulnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VulnerApplication.class, args);
	}

	public Collection<? extends GrantedAuthority> getAuthorities(String username) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(ROLE_USER));
		if(username.equals("admin")){
			authorities.add(new SimpleGrantedAuthority(ROLE_ADMIN));
		}
		return authorities;
	}
	@Bean
	public UserDetailsService userDetailsService(UserRepository userRepository) {
		return username -> userRepository.findById(username)
				.map(entity -> new User(entity.getUsername(), entity.getPassword(), getAuthorities(entity.getUsername())))
				.orElseThrow(() -> new UsernameNotFoundException(username));
	}

	@Bean
	public PasswordEncoder passwordEncoder(){
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
		CookieCsrfTokenRepository tokenRepository = CookieCsrfTokenRepository.withHttpOnlyFalse();
		XorCsrfTokenRequestAttributeHandler delegate = new XorCsrfTokenRequestAttributeHandler();
		delegate.setCsrfRequestAttributeName("_csrf");
		CsrfTokenRequestHandler requestHandler = delegate::handle;
		return http
				.httpBasic(basic -> basic.realmName("vulnerapp"))
				.csrf(cfg -> cfg.csrfTokenRepository(tokenRepository)
						.csrfTokenRequestHandler(requestHandler))
				.authorizeHttpRequests(auth ->
						auth.requestMatchers("/api/user/whoami", "/api/blog/", "/").permitAll().requestMatchers("/api/**").authenticated().anyRequest().permitAll()
				)
				.headers(headers ->
						headers.xssProtection(withDefaults())
								.contentSecurityPolicy("default-src 'self'")// Specify your desired CSP policy here
				).build();
	}
}
